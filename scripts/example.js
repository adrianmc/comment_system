/** @jsx React.DOM */

/*
 * TODO - React.autoBind is now an NO-OP, can remove it and just put the function down
 */

var converter = new Showdown.converter();

/*
 * the CommentBox Component - this is the TOP component
 */
var CommentBox = React.createClass({
  
  // FUNCTION: loads comments from server via AJAX
  loadCommentsFromServer: React.autoBind(function() {
    $.ajax({
      url: this.props.url,
      success: function(data) {
        this.setState({data: data});  // set the data from server's record of comments
      }.bind(this)
    });
  }),

  // FUNCTION: makes a POST request to the server
  // 1. Sends comment data that the user wants to post to the server
  // 2. This function is also passed down as a callback to its child
  //    component known as CommentForm
  handleCommentSubmit: React.autoBind(function(comment) {
    
    var comments = this.state.data;   // take the data passed from the child
    comments.push(comment);           // add this data to the array of comments
    this.setState({data: comments});  // set the state with the updated array of comments
    
    // make the AJAX call to update the server
    $.ajax({
      url: this.props.url,
      type: 'POST',
      data: comment,                  // we are only sending the single comment up to the server
      success: function(data) {
        this.setState({data: data});  // update state from server's record of comments
      }.bind(this)
    });
  }),

  // this function initializes an array with the component 
  getInitialState: function() {
    return {data: []};
  },
  // this function is run right after the component has mounted
  componentDidMount: function() {
    this.loadCommentsFromServer();                                      // load the comments from server
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);  // continuously poll the server
  },
  render: function() {

    // pass the array of comments into the CommentList child so it can be displayed
    // pass a function to CommentForm so it can handle a form submission
    return (
      <div class="commentBox">
        <h1>Comments</h1>
        <CommentList data={this.state.data} />
        <CommentForm onCommentSubmit={this.handleCommentSubmit} />
      </div>
    );
  }
});

/*
 * the CommentList component - list of Comment components
 */
var CommentList = React.createClass({
  render: function() {

    // assign to commentNodes, each comment...
    // by sending variables into the Comment component
    // and getting what it returns
    var commentNodes = this.props.data.map(function (comment) {
      return <Comment author={comment.author}>{comment.text}</Comment>;
    });

    // display commentNodes inside the commentList div
    return <div class="commentList">{commentNodes}</div>;
  }
});

/*
 * the Comment component - each individual comment
 */
var Comment = React.createClass({
  render: function() {
    var rawMarkup = converter.makeHtml(this.props.children.toString()); // first convert markdown to raw HTML

    // we have to use dangerouslySetInnerHTML because our markdown converter outputs in raw markup
    return (
      <div class="comment">
        <h2 class="commentAuthor">{this.props.author}</h2>
        <span dangerouslySetInnerHTML={{__html: rawMarkup}} />
      </div>
    );
  }
});

/*
 * the CommentForm component - for submitting a comment
 */
var CommentForm = React.createClass({
  
  // FUNCTION: handle the submission of the form
  handleSubmit: React.autoBind(function() {
    var author = this.refs.author.getDOMNode().value.trim();
    var text = this.refs.text.getDOMNode().value.trim();
    
    // Do nothing if either of the boxes are empty
    if (text.length === 0 || author.length === 0) {
      return false;
    }

    // Calls the callback function from the parent component
    // The parent will do the necessary work to post the 
    // comment to the server
    this.props.onCommentSubmit({author: author, text: text});
    
    // Clear the text boxes
    this.refs.author.getDOMNode().value = '';
    this.refs.text.getDOMNode().value = '';
    return false;
  }),
  render: function() {

    // when the form is submitted, we will call handleSubmit
    return (
      <form className="commentForm" onSubmit={this.handleSubmit}>
        <input type="text" placeholder="Your name" ref="author" />
        <textarea cols="40" rows="5" placeholder="Say something..." ref="text" />
        <input type="submit" value="Post" />
      </form>
    );
  }
});

// final rendering of the CommentBox component
React.renderComponent(
  <CommentBox url="/comments.json" pollInterval={500} />,
  document.getElementById('container')
);