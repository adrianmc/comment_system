var fs = require('fs');

var express = require('express');
var app = express();

// initialize a record of comments
var comments = [{author: 'Adrian Li', text: 'Hey there! Start typing your \
	comments below. Anything you type is automatically updated in all other \
	instances. Try opening multiple tabs!'}];

app.use('/', express.static(__dirname));
app.use(express.bodyParser());

// when clients make a GET request to '/comments.json'
app.get('/comments.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(comments));	// send back the server's record of comments
});

// when clients make a POST request to '/comments.json'
app.post('/comments.json', function(req, res) {
  comments.push(req.body);				// append the body of the request to our comment record
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(comments));	// send back the server's record of comments
});

app.listen(process.env.PORT || 5000);