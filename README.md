# Comment System Exercise

This is based on the React comment box example from [the React tutorial](http://facebook.github.io/react/docs/tutorial.html).

To know what's going on, you'll probably do best to go to that tutorial first. Everything operates in components. This Quora question is also a good introduction: [here](http://www.quora.com/React-JS-Library/How-is-Facebooks-React-JavaScript-library).

## Component Structure
```
CommentBox
|--CommentList
   |--Comment
   |--Comment
   |--Comment
   ...
|--CommentForm
```

## Requirements
You need to have nodejs installed

## To use

Install dependencies and load server:
```
npm install express
node server.js
```